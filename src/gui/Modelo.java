package gui;

import base.Alumno;
import base.Asignatura;
import base.CommonSetting;
import base.Profesor;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.swing.event.ListSelectionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo implements CommonSetting{

    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;
    private MongoCollection collectionAlumno;
    private MongoCollection collectionProfesor;
    private MongoCollection collectionAsignatura;

    public void connect(){
        mongoClient = new MongoClient();
        try {
            mongoDatabase = mongoClient.getDatabase(DATABASE_NAME);

        }catch (Exception e){

        }
        collectionAlumno = mongoDatabase.getCollection(COlLECTION_ALUMNO);
        collectionAsignatura = mongoDatabase.getCollection(COLLECTION_ASIGNATURAS);
        collectionProfesor = mongoDatabase.getCollection(COLLECTION_PROFESORES);
    }
    public void discconect(){
        mongoClient.close();
    }

    public void insertOne(Object o){
        if(o.getClass().equals(Alumno.class)){
            collectionAlumno.insertOne(objectToDocument(o));
        }
        if(o.getClass().equals(Profesor.class)){
            collectionProfesor.insertOne(objectToDocument(o));
        }
        if(o.getClass().equals(Asignatura.class)){
            collectionAsignatura.insertOne(objectToDocument(o));
        }
    }
    public void deleteOne(Object o){
        if(o.getClass().equals(Alumno.class)){
            collectionAlumno.deleteOne(objectToDocument(o));
        }
        if(o.getClass().equals(Profesor.class)){
            collectionProfesor.deleteOne(objectToDocument(o));
        }
        if(o.getClass().equals(Asignatura.class)){
            collectionAsignatura.deleteOne(objectToDocument(o));
        }
    }
    public void updateOne(Object o){
        if(o.getClass().equals(Alumno.class)){
            Alumno a = (Alumno) o;
            collectionAlumno.replaceOne(new Document("_id",a.getId()),objectToDocument(a));
        }
        if(o.getClass().equals(Profesor.class)){
            Profesor p = (Profesor) o;
            collectionProfesor.replaceOne(new Document("_id",p.getId()),objectToDocument(p));
        }
        if(o.getClass().equals(Asignatura.class)){
            Asignatura as = (Asignatura) o;
            collectionAsignatura.replaceOne(new Document("_id", as.getId()),objectToDocument(as));
        }

        //collectionAlumno.replaceOne(new Document("_id",o.getId()),objectToDocument(o));
    }

    public Document objectToDocument(Object o){
        Document document;
        if(o.getClass().equals(Alumno.class)){
            Alumno alumno = (Alumno) o;
            document = new Document();
            document.append(KEY_NOMBRE, alumno.getNombre());
            document.append(KEY_APELLIDOS,alumno.getApellido());
            document.append(KEY_ALUMNO_EDAD, alumno.getEdad());
            return document;
        }
        if(o.getClass().equals(Profesor.class)){
            Profesor profesor = (Profesor) o;
            document = new Document();
            document.append(KEY_NOMBRE, profesor.getNombre());
            document.append(KEY_APELLIDOS, profesor.getApellido());
            document.append(KEY_PROFESOR_EXPERIENCIA, profesor.getExperiencia());
            return document;
        }
        if(o.getClass().equals(Asignatura.class)){
            Asignatura asignatura = (Asignatura) o;
            document = new Document();
            document.append(KEY_NOMBRE , asignatura.getNombre());
            document.append(KEY_ASIG_DESCRIPCION, asignatura.getDescripcion());
            document.append(KEY_ASIGNATURA_HORAS, asignatura.getHoras());
            return document;
        }
        return null;
    }

    public Alumno documentToAlumno(Document document){
        Alumno a  = new Alumno();
        a.setId(document.getObjectId(KEY_ID));
        a.setNombre(document.getString(KEY_NOMBRE));
        a.setApellido(document.getString(KEY_APELLIDOS));
        a.setEdad(document.getInteger(KEY_ALUMNO_EDAD));
        return a;
    }
    public Profesor documentToProfesor(Document document){
        Profesor p  = new Profesor();
        p.setId(document.getObjectId(KEY_ID));
        p.setNombre(document.getString(KEY_NOMBRE));
        p.setApellido(document.getString(KEY_APELLIDOS));
        p.setExperiencia(document.getInteger(KEY_PROFESOR_EXPERIENCIA));
        return p;
    }
    public Asignatura documentToAsignatura(Document document){
        Asignatura a  = new Asignatura();
        a.setId(document.getObjectId(KEY_ID));
        a.setNombre(document.getString(KEY_NOMBRE));
        a.setDescripcion(document.getString(KEY_ASIG_DESCRIPCION));
        a.setHoras(document.getInteger(KEY_ASIGNATURA_HORAS));
        return a;
    }
     public List<Alumno> getAlumnos(){
        ArrayList<Alumno> alumnos = new ArrayList<>();
        Iterator<Document>it = collectionAlumno.find().iterator();
        while (it.hasNext()){
            alumnos.add(documentToAlumno(it.next()));
        }
        return alumnos;
     }
    public List<Profesor> getProfesores(){
        ArrayList<Profesor> profesors = new ArrayList<>();
        Iterator<Document>it = collectionProfesor.find().iterator();
        while (it.hasNext()){
            profesors.add(documentToProfesor(it.next()));
        }
        return profesors;
    }
    public List<Asignatura> getAsignatura(){
        ArrayList<Asignatura> asignaturas = new ArrayList<>();
        Iterator<Document>it = collectionAsignatura.find().iterator();
        while (it.hasNext()){
            asignaturas.add(documentToAsignatura(it.next()));
        }
        return asignaturas;
    }
    private ArrayList<Object> darObjects(String collection, Document query){
        ArrayList<Object> objects = new ArrayList<>();

        if (collection.equals(COlLECTION_ALUMNO)){
            Iterator<Document> iterator = collectionAlumno.find(query).iterator();
            while (iterator.hasNext()){
                objects.add(documentToAlumno(iterator.next()));
            }
        }else if (collection.equals(COLLECTION_ASIGNATURAS)){
            Iterator<Document> iterator = collectionAsignatura.find(query).iterator();
            while (iterator.hasNext()){
                objects.add(documentToAsignatura(iterator.next()));
            }
        }else if (collection.equals(COLLECTION_PROFESORES)){
            Iterator<Document> iterator = collectionProfesor.find(query).iterator();
            while (iterator.hasNext()){
                objects.add(documentToProfesor(iterator.next()));
            }
        }/*else if (collection.equals("Barcos")){
            Iterator<Document> iterator = shipsCollection.find(query).iterator();
            while (iterator.hasNext()){
                objects.add(documentToShip(iterator.next()));
            }
        }*/

        return objects;
    }
    public ArrayList<Object> getObjectBy(String text, String collection){

        Document query = new Document();
        ArrayList<Document> criterios = new ArrayList<>();
        if (collection.equals(COlLECTION_ALUMNO)){
            criterios.add(new Document(KEY_NOMBRE, new Document("$regex", "/*" + text + "/*")));
            criterios.add(new Document(KEY_APELLIDOS, new Document("$regex", "/*" + text + "/*")));
            //criterios.add(new Document("tag", new Document("$regex", "/*" + text + "/*")));
        }else if (collection.equals(COLLECTION_ASIGNATURAS)){
            criterios.add(new Document(KEY_NOMBRE, new Document("$regex", "/*" + text + "/*")));
            criterios.add(new Document(KEY_ASIG_DESCRIPCION, new Document("$regex", "/*" + text + "/*")));

        }else if (collection.equals(COLLECTION_PROFESORES)){
            criterios.add(new Document(KEY_NOMBRE, new Document("$regex", "/*" + text + "/*")));
            criterios.add(new Document(KEY_APELLIDOS, new Document("$regex", "/*" + text + "/*")));
        }
        /*else if (collection.equals("Barcos")){
            criterios.add(new Document("nombre", new Document("$regex", "/*" + text + "/*")));
        }*/
        query.append("$or", criterios);

        return darObjects(collection, query);
    }
    public ArrayList<Object> getObjectBy(int numero, String collection){

        Document query = new Document();
        ArrayList<Document> criterios = new ArrayList<>();

        if (collection.equals(COlLECTION_ALUMNO)){
            criterios.add(new Document(KEY_ALUMNO_EDAD,  numero ));
            //criterios.add(new Document("canon", numero ));
        }else if (collection.equals(COLLECTION_ASIGNATURAS)){
            criterios.add(new Document(KEY_ASIGNATURA_HORAS, numero ));
            //criterios.add(new Document("velocidad", numero ));
        }else if (collection.equals(COLLECTION_PROFESORES)){
            criterios.add(new Document(KEY_PROFESOR_EXPERIENCIA, numero ));
            //criterios.add(new Document("canon_numbers", numero ));
        }
        query.append("$or", criterios);

        return darObjects(collection, query);
    }
}
