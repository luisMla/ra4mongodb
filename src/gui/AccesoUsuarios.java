package gui;

import javax.swing.*;
import java.awt.event.*;

public class AccesoUsuarios extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfUsuario;
    private JPasswordField pfPass;
    public final static int ACCEPT = 0;
    public final static int CANCELADO = 1;

    private String usuario;
    private String password;
    private int estado ;
    private boolean admin = false;


    public AccesoUsuarios() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void onOK() {
        // add your code here
        estado = ACCEPT;
        usuario = tfUsuario.getText();
        password = String.valueOf(pfPass.getPassword());
        if(usuario.equals("admin")&&password.equals("admin"))
            admin = true;
        if((!usuario.equals("admin")||!password.equals("admin"))&&((!usuario.equals("usuario")||!password.equals("usuario"))))
            estado = ERROR;
       // System.out.println("estado "+estado);
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        estado = CANCELADO;
        dispose();
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    /*  public static void main(String[] args) {
        AccesoUsuarios dialog = new AccesoUsuarios();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }*/

}
