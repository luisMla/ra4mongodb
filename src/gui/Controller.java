package gui;

import base.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.io.File;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Controller implements CommonSetting , ActionListener, ListSelectionListener, WindowListener, Runnable, KeyListener{
    private Modelo m;
    private VistaPrincipal ui;
    private boolean bol = true;
    private Alumno alumnoSelect;
    private Profesor profesorSelect;
    private Asignatura asignaturaSelect;

    private DefaultListModel<Alumno> dlmAlumno;
    private DefaultListModel<Profesor> dlmProfesor;
    private DefaultListModel<Asignatura> dlmAsignatura;
    private DefaultListModel<Object>dlmObject;
    private ArrayList<Alumno> alumnos;
    private ArrayList<Profesor>profesors;
    private ArrayList<Asignatura>asignaturas;

    private Thread thread;

    public Controller(Modelo m, VistaPrincipal ui) {
        this.m = m;
        this.ui = ui;
        launchInit();
        enableAll();
        initActionListener(this);
        initListSelectionListener(this);
        initListSelectionListener(this);
        initWindowListener(this);
        initKeyListener(this);
        constructElements();
        m.connect();
        enableAll();
        thread = new Thread(this);
        thread.start();
    }
    private void constructElements(){
        dlmAlumno = new DefaultListModel<>();
        dlmAsignatura = new DefaultListModel<>();
        dlmProfesor = new DefaultListModel<>();
        dlmObject = new DefaultListModel<>();

        ui.listAlumnos.setModel(dlmAlumno);
        ui.listAsignaturas.setModel(dlmAsignatura);
        ui.listProfesor.setModel(dlmProfesor);

        alumnos = new ArrayList<>();
        profesors = new ArrayList<>();
        asignaturas = new ArrayList<>();
    }
    private void launchInit(){
        AccesoUsuarios accesoUsuarios = new AccesoUsuarios();
        if(accesoUsuarios.getEstado() == AccesoUsuarios.ACCEPT){

        }else if (accesoUsuarios.getEstado() == AccesoUsuarios.ERROR){
            JOptionPane.showMessageDialog(null, "El usuario o la contraseña no son correctas",
                    "ERROR",JOptionPane.ERROR_MESSAGE);
            launchInit();
        }else if(accesoUsuarios.getEstado()== AccesoUsuarios.CANCELADO){
            System.exit(0);
        }
    }
    private void enableAll(){
        bol = !bol;
        ui.btAltaAlumno.setEnabled(bol);
        ui.btBajaAlumno.setEnabled(bol);
        ui.btModificarAlumno.setEnabled(bol);

        ui.btAltaAs.setEnabled(bol);
        ui.btBajaAs.setEnabled(bol);
        ui.btModificarAs.setEnabled(bol);

        ui.btAltaPro.setEnabled(bol);
        ui.btBajaPro.setEnabled(bol);
        ui.btModificarPro.setEnabled(bol);

        ui.btImportarXml.setEnabled(bol);
        ui.btEsportXml.setEnabled(bol);
        ui.rbAuto.setEnabled(bol);
        ui.comboBox1.setEnabled(bol);
    }
    private void initActionListener(ActionListener actionListener){
        ui.btAltaAlumno.addActionListener(actionListener);
        ui.btModificarAlumno.addActionListener(actionListener);
        ui.btBajaAlumno.addActionListener(actionListener);

        ui.btAltaAs.addActionListener(actionListener);
        ui.btModificarAs.addActionListener(actionListener);
        ui.btBajaAs.addActionListener(actionListener);

        ui.btAltaPro.addActionListener(actionListener);
        ui.btModificarPro.addActionListener(actionListener);
        ui.btBajaPro.addActionListener(actionListener);

        ui.btEsportXml.addActionListener(actionListener);
        ui.btImportarXml.addActionListener(actionListener);
        ui.rbAuto.addActionListener(actionListener);
        ui.btSalir.addActionListener(actionListener);

    }
    private void initListSelectionListener(ListSelectionListener listSelectionListener){
        ui.listAlumnos.addListSelectionListener(listSelectionListener);
        ui.listProfesor.addListSelectionListener(listSelectionListener);
        ui.listAsignaturas.addListSelectionListener(listSelectionListener);
        ui.listResultado.addListSelectionListener(listSelectionListener);
    }

    private void initKeyListener(KeyListener keyListener){
        ui.tfBuscar.addKeyListener(keyListener);
    }
    private void initWindowListener(WindowListener windowListener){
        ui.frame.addWindowListener(windowListener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser jFileChooser;
        String action="";
        switch (e.getActionCommand()){
            case AC_ALTA_ALUMNO:
                Alumno al = new Alumno();
                al.setNombre(ui.tfNombreAlumno.getText());
                al.setApellido(ui.tfApellidosAlumno.getText());
                al.setEdad(Integer.parseInt(ui.tfEdadAlumno.getText()));
                m.insertOne(al);
                action = "Alta Alumno "+ al.toString();
                break;
            case AC_DELETE_ALUMNO:
                if (alumnoSelect != null) {
                    m.deleteOne(alumnoSelect);
                    action = "Borrado Alumno " + alumnoSelect.toString();
                    dlmAlumno.removeElement(alumnoSelect);
                }
                break;
            case AC_MODI_ALUMNO:
                if (alumnoSelect != null){
                    alumnoSelect.setNombre(ui.tfNombreAlumno.getText());
                    alumnoSelect.setApellido(ui.tfApellidosAlumno.getText());
                    alumnoSelect.setEdad(Integer.parseInt(ui.tfEdadAlumno.getText()));
                    m.updateOne(alumnoSelect);
                    action = "Modificado Alumno " +alumnoSelect.toString();
                }
                break;
            case AC_ALTA_PROFESOR:
                Profesor profesor = new Profesor();
                profesor.setNombre(ui.tfNombreProfesor.getText());
                profesor.setApellido(ui.tfApellidoProfesor.getText());
                profesor.setExperiencia(Integer.parseInt(ui.tfExperiencia.getText()));
                m.insertOne(profesor);
                action = "Alta Profesor " + profesor.toString();
                break;
            case AC_DELETE_PROFESOR:
                if (profesorSelect != null){
                    m.deleteOne(profesorSelect);
                    action = "Borrado Profesor " + profesorSelect.toString();
                    dlmProfesor.removeElement(profesorSelect);
                }
                break;
            case AC_MODI_PROFESOR:
                if (profesorSelect != null) {
                    profesorSelect.setNombre(ui.tfNombreProfesor.getText());
                    profesorSelect.setApellido(ui.tfApellidoProfesor.getText());
                    profesorSelect.setExperiencia(Integer.parseInt(ui.tfExperiencia.getText()));
                    m.updateOne(profesorSelect);
                    action = "Modificado Profesor " + profesorSelect.toString();
                }
                break;
            case AC_ALTA_ASIG:
                Asignatura asignatura = new Asignatura();
                asignatura.setNombre(ui.tfNombreAs.getText());
                asignatura.setDescripcion(ui.tfDescripcion.getText());
                asignatura.setHoras(Integer.parseInt(ui.tfHoras.getText()));
                m.insertOne(asignatura);
                action = "Alta Asignatura " + asignatura.toString();
                break;
            case AC_DELETE_ASIG:
                if (asignaturaSelect != null){
                    m.deleteOne(asignaturaSelect);
                    action = "Borrado Asignatura " + asignaturaSelect.toString();
                    dlmAsignatura.removeElement(asignaturaSelect);
                }
                break;
            case AC_MODI_ASIG:
                if (asignaturaSelect != null){
                    asignaturaSelect.setNombre(ui.tfNombreAs.getText());
                    asignaturaSelect.setDescripcion(ui.tfDescripcion.getText());
                    asignaturaSelect.setHoras(Integer.parseInt(ui.tfHoras.getText()));
                    m.updateOne(asignaturaSelect);
                    action = "Modificado Asignatura " + asignaturaSelect.toString();
                }
                break;
            case AC_EX_XML:
                jFileChooser = new JFileChooser();
                jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                jFileChooser.setFileFilter(new FileNameExtensionFilter("XML","xml"));
                if((jFileChooser.showSaveDialog(null)) == JFileChooser.APPROVE_OPTION){
                    File file = jFileChooser.getSelectedFile();
                    ArrayList<Object> objects = new ArrayList<>();
                    objects.addAll(asignaturas);
                    objects.addAll(profesors);
                    objects.addAll(alumnos);
                    DbXml.exportarXML(objects,file);
                    action = "Exportado en xml en " + file.getAbsoluteFile();
                }
                break;
            case AC_IMPXML:
                jFileChooser = new JFileChooser();
                jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                jFileChooser.setFileFilter(new FileNameExtensionFilter("XML", "xml"));
                if(jFileChooser.showOpenDialog(null)== JFileChooser.APPROVE_OPTION){
                    ArrayList<Object> objects = new ArrayList<>();
                    objects = (ArrayList<Object>) DbXml.importarXml(jFileChooser.getSelectedFile());
                    for (Object o : objects) {
                        m.insertOne(o);
                    }
                }
                action = "Importado datos desde xml";
                break;
            case AC_AUTO:
                if(ui.rbAuto.isSelected()){
                    launcher(this);
                    action = "Ultima accion activar Auto Refescado";
                }else {
                    action = "Ultima accion desactivar Auto Refrescado";
                }
                break;
            case AC_SALIR:
                m.discconect();
                System.exit(0);
                break;
        }
        if(!ui.rbAuto.isSelected()){
            thread = new Thread(this);
            thread.start();
        }
        ultimaAccion(action);
        cleanFields();
    }

    private void cleanFields(){
            ui.tfNombreAlumno.setText("");
            ui.tfApellidosAlumno.setText("");
            ui.tfEdadAlumno.setText("");

            ui.tfNombreProfesor.setText("");
            ui.tfApellidoProfesor.setText("");
            ui.tfExperiencia.setText("");

            ui.tfNombreAs.setText("");
            ui.tfDescripcion.setText("");
            ui.tfHoras.setText("");

            ui.tfBuscar.setText("");

    }

    private void launcher (Runnable r){
        Thread launcher = new Thread(new Runnable() {
            @Override
            public void run() {
               while (ui.rbAuto.isSelected()){
                   Thread hilo = new Thread(r);
                   hilo.start();
                   System.out.println("launcher lanza rubable"+ Thread.currentThread());
                   try {
                       System.out.println("SLEEP");
                       Thread.sleep(TIME_SLEEP);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }
               System.out.println("fuera del while" + Thread.currentThread());
            }
        });
        launcher.start();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        String action = "";
        if(e.getValueIsAdjusting()){
            if (e.getSource().equals(ui.listAlumnos)){
                alumnoSelect = (Alumno) ui.listAlumnos.getSelectedValue();
                paintObject(alumnoSelect);
                action = "Selección Alumno : "+alumnoSelect.toString();
            }
            if (e.getSource().equals(ui.listAsignaturas)){
                asignaturaSelect = (Asignatura) ui.listAsignaturas.getSelectedValue();
                paintObject(asignaturaSelect);
                action = "Selección Asignatura : "+asignaturaSelect.toString();
            }
            if (e.getSource().equals(ui.listProfesor)){
                profesorSelect = (Profesor) ui.listProfesor.getSelectedValue();
                paintObject(profesorSelect);
                action = "Selección Profesor : "+profesorSelect.toString();
            }
            if (e.getSource().equals(ui.listResultado)){
                action = "buscando elementos ";
                if(ui.listResultado.getSelectedValue().getClass().equals(Alumno.class)){
                    alumnoSelect = (Alumno) ui.listResultado.getSelectedValue();
                    ui.listAlumnos.setSelectedValue(alumnoSelect,false);
                    System.out.println( alumnoSelect.toString());
                    action += "seleccionado Alumno " + alumnoSelect.toString();
                }
                if(ui.listResultado.getSelectedValue().getClass().equals(Asignatura.class)){
                    asignaturaSelect = (Asignatura) ui.listResultado.getSelectedValue();
                    ui.listAsignaturas.setSelectedValue(asignaturaSelect,false);
                    System.out.println(asignaturaSelect.toString());
                    action += "seleccionado Asignatura " + asignaturaSelect.toString();

                }
                if(ui.listResultado.getSelectedValue().getClass().equals(Profesor.class)){
                    profesorSelect = (Profesor) ui.listResultado.getSelectedValue();
                    ui.listProfesor.setSelectedValue(profesorSelect,false);
                    System.out.println(profesorSelect.toString());
                    action += "seleccionado Profesor " + profesorSelect.toString();

                }
            }
            ultimaAccion(action);
        }

    }
    private void paintObject(Object o){
        if(o.getClass().equals(Alumno.class)){
            Alumno alumno = (Alumno) o;
            ui.tfNombreAlumno.setText(alumno.getNombre());
            ui.tfApellidosAlumno.setText(alumno.getApellido());
            ui.tfEdadAlumno.setText(String.valueOf(alumno.getEdad()));
        }
        if(o.getClass().equals(Profesor.class)){
            Profesor profesor = (Profesor) o;
            ui.tfNombreProfesor.setText(profesor.getNombre());
            ui.tfApellidoProfesor.setText(profesor.getApellido());
            ui.tfExperiencia.setText(String.valueOf(profesor.getExperiencia()));
        }
        if(o.getClass().equals(Asignatura.class)){
            Asignatura asignatura = (Asignatura) o;
            ui.tfNombreAs.setText(asignatura.getNombre());
            ui.tfDescripcion.setText(asignatura.getDescripcion());
            ui.tfHoras.setText(String.valueOf(asignatura.getHoras()));
        }
    }
    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        m.discconect();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void run() {
        try {
            alumnos = (ArrayList<Alumno>) m.getAlumnos();
            feedList(alumnos);
        }catch (Exception e){
            System.out.println("cat alumno");
            Alumno alumno = new Alumno();
            alumno.setEdad(0);
            alumno.setApellido("");
            alumno.setNombre("");
            m.insertOne(alumno);
            m.deleteOne(alumno);
        }
        try {
            asignaturas = (ArrayList<Asignatura>) m.getAsignatura();
            feedList(asignaturas);
        }catch (Exception e){
            System.out.println("cat asignatura");
            Asignatura asignatura = new Asignatura();
            asignatura.setHoras(0);
            asignatura.setDescripcion("");
            asignatura.setNombre("");
            m.insertOne(asignatura);
            m.deleteOne(asignatura);
        }
        try {
            profesors = (ArrayList<Profesor>) m.getProfesores();
            feedList(profesors);
        }catch (Exception e){
            System.out.println("car profesor");
            Profesor profesor = new Profesor();
            profesor.setNombre("");
            profesor.setApellido("");
            profesor.setExperiencia(0);
            m.insertOne(profesor);
            m.deleteOne(profesor);
        }
        ui.lblEstado.setText("Ultima Actualización  "+ LocalDateTime.now().toString() +" Estado 'Conectado'"+
                " Total Alumnos: " + alumnos.size()+ " Total Profesores: " + profesors.size()+
                " Total Asignaturas: " + asignaturas.size());
        //System.out.println("Dentro de runable clase this "+ Thread.currentThread());
    }
    private void feedList(List objects){
        if(objects.size()==0)
            return;
        if(objects.size()!=0){
            if(objects.get(0).getClass().equals(Alumno.class)){
                dlmAlumno.clear();
                for (Object al: objects ) {
                    dlmAlumno.addElement((Alumno) al);
                }
                ui.listAlumnos.setModel(dlmAlumno);
            }
            if(objects.get(0).getClass().equals(Profesor.class)){
                dlmProfesor.clear();
                for (Object pr : objects) {
                    dlmProfesor.addElement((Profesor) pr);
                }
                ui.listProfesor.setModel(dlmProfesor);
            }
            if(objects.get(0).getClass().equals(Asignatura.class)){
                dlmAsignatura.clear();
                for (Object as : objects){
                    dlmAsignatura.addElement((Asignatura) as);
                }
                ui.listAsignaturas.setModel(dlmAsignatura);
            }
        }
    }
    private void feedList(List objects, boolean a){
        dlmObject.clear();
        for (Object o : objects) {
            dlmObject.addElement(o);
        }
        ui.listResultado.setModel(dlmObject);
    }
    private void ultimaAccion(String Action){
        ui.lblUltimaAccion.setText("Ultima acción: "+ Action + "  " +LocalDateTime.now().toString());
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        ArrayList<Object> objects = new ArrayList<>();
        int numero = 0;
        if(e.getSource().equals(ui.tfBuscar)){
            if(ui.tfBuscar.getText().equals(""))
                return;
            if(ui.comboBox1.getSelectedItem().equals("Todas las Colecciones")){
                try {
                    numero = Integer.parseInt(ui.tfBuscar.getText());
                    objects.addAll(m.getObjectBy(numero,CommonSetting.COlLECTION_ALUMNO));
                    objects.addAll(m.getObjectBy(numero,CommonSetting.COLLECTION_ASIGNATURAS));
                    objects.addAll(m.getObjectBy(numero,CommonSetting.COLLECTION_PROFESORES));

                }catch (Exception er){
                    objects.addAll(m.getObjectBy(ui.tfBuscar.getText(),CommonSetting.COlLECTION_ALUMNO));
                    objects.addAll(m.getObjectBy(ui.tfBuscar.getText(),CommonSetting.COLLECTION_ASIGNATURAS));
                    objects.addAll(m.getObjectBy(ui.tfBuscar.getText(),CommonSetting.COLLECTION_PROFESORES));
                }
            }
            if(ui.comboBox1.getSelectedItem().equals("Alumnos")){
                try {
                    numero = Integer.parseInt(ui.tfBuscar.getText());
                    objects.addAll(m.getObjectBy(numero,CommonSetting.COlLECTION_ALUMNO));

                }catch (Exception er){
                    objects.addAll(m.getObjectBy(ui.tfBuscar.getText(),CommonSetting.COlLECTION_ALUMNO));
                }
            }
            if(ui.comboBox1.getSelectedItem().equals("Profesor")){
                try {
                    numero = Integer.parseInt(ui.tfBuscar.getText());
                    objects.addAll(m.getObjectBy(numero,CommonSetting.COLLECTION_PROFESORES));

                }catch (Exception er){
                    objects.addAll(m.getObjectBy(ui.tfBuscar.getText(),CommonSetting.COLLECTION_PROFESORES));
                }
            }
            if(ui.comboBox1.getSelectedItem().equals("Asignatura")){
                try {
                    numero = Integer.parseInt(ui.tfBuscar.getText());
                    objects.addAll(m.getObjectBy(numero,CommonSetting.COLLECTION_ASIGNATURAS));

                }catch (Exception er){
                    objects.addAll(m.getObjectBy(ui.tfBuscar.getText(),CommonSetting.COLLECTION_ASIGNATURAS));
                }
            }
            feedList(objects,true);
        }
    }
}
