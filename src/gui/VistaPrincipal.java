package gui;

import base.CommonSetting;

import javax.swing.*;

public class VistaPrincipal implements CommonSetting{
    public JFrame frame;
    public JPanel panel1;
    public JLabel lblNombre;
    public JLabel lblApellidos;
    public JLabel lblEdad;
    public JTextField tfNombreAlumno;
    public JTextField tfApellidosAlumno;
    public JTextField tfEdadAlumno;
    public JButton btAltaAlumno;
    public JButton btBajaAlumno;
    public JButton btModificarAlumno;
    public JList listAlumnos;
    public JLabel lblNombreProfesor;
    public JLabel lblApellidosProfesor;
    public JLabel lblExperiencia;
    public JButton btAltaPro;
    public JButton btBajaPro;
    public JButton btModificarPro;
    public JTextField tfNombreProfesor;
    public JTextField tfApellidoProfesor;
    public JTextField tfExperiencia;
    public JList listProfesor;
    public JLabel lblNombreAs;
    public JLabel lblDescripcion;
    public JLabel lblHoras;
    public JTextField tfNombreAs;
    public JTextField tfDescripcion;
    public JTextField tfHoras;
    public JButton btAltaAs;
    public JButton btBajaAs;
    public JButton btModificarAs;
    public JList listAsignaturas;
    public JLabel lblEstado;
    public JLabel lblUltimaAccion;
    public JComboBox comboBox1;
    public JList listResultado;
    public JTextField tfBuscar;
    public JButton btEsportXml;
    public JButton btImportarXml;
    public JRadioButton rbAuto;
    public JButton btSalir;

    public VistaPrincipal() {
        frame = new JFrame("VistaPrincipal");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addActionComand();
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    private void addActionComand(){
        btAltaAlumno.setActionCommand(AC_ALTA_ALUMNO);
        btModificarAlumno.setActionCommand(AC_MODI_ALUMNO);
        btBajaAlumno.setActionCommand(AC_DELETE_ALUMNO);

        btAltaPro.setActionCommand(AC_ALTA_PROFESOR);
        btBajaPro.setActionCommand(AC_DELETE_PROFESOR);
        btModificarPro.setActionCommand(AC_MODI_PROFESOR);

        btAltaAs.setActionCommand(AC_ALTA_ASIG);
        btBajaAs.setActionCommand(AC_DELETE_ASIG);
        btModificarAs.setActionCommand(AC_MODI_ASIG);

        btEsportXml.setActionCommand(AC_EX_XML);
        btImportarXml.setActionCommand(AC_IMPXML);

        rbAuto.setActionCommand(AC_AUTO);
        btSalir.setActionCommand(AC_SALIR);

    }


}
