package base;

public interface CommonSetting {
    public static final String DATABASE_NAME = "ra4db";
    public static final String COlLECTION_ALUMNO = "alumnos";
    public static final String COLLECTION_PROFESORES = "profesores";
    public static final String COLLECTION_ASIGNATURAS = "asignaturas";
    //public static final String COLLECTION_LIBRO = "libros";
    //public static final String COLLECTION_AUTORES = "autores";

    public static final String KEY_ID = "_id";
    public static final String KEY_NOMBRE = "nombre";
    public static final String KEY_APELLIDOS = "apellido";

    public static final String KEY_ALUMNO_EDAD = "edad";
    public static final String KEY_PROFESOR_EXPERIENCIA = "experiencia";
    public static final String KEY_ASIGNATURA_HORAS = "horas";
    public static final String KEY_ASIG_DESCRIPCION = "descripcion";

    public static final String TAG_ALUMNO = "alumno";
    public static final String TAG_PROFESOR = "profesor";
    public static final String TAG_ASIGNATURA = "asignatura";

    public static final  String AC_ALTA_ALUMNO = "AAL";
    public static final String AC_MODI_ALUMNO = "MAL";
    public static final String AC_DELETE_ALUMNO = "DAL";
    public static final String AC_ALTA_PROFESOR = "APR";
    public static final String AC_MODI_PROFESOR = "MPR";
    public static final String AC_DELETE_PROFESOR = "DPR";
    public static final String AC_ALTA_ASIG = "AAS";
    public static final String AC_MODI_ASIG = "MAS";
    public static final String AC_DELETE_ASIG = "DAS";
    public static final String AC_EX_XML = "EXPXML";
    public static final String AC_IMPXML = "IMPXML";
    public static final String AC_AUTO = "AUTO";
    public static final String AC_SALIR = "SALIR";
    public static final int TIME_SLEEP = 10000;


}
