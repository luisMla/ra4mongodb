package base;
import java.io.*;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bson.types.ObjectId;
import org.xml.sax.SAXException;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.DOMImplementation;

import org.w3c.dom.Text;
public class DbXml implements CommonSetting{

    public static void exportarXML(List<Object> gestorObject, File fichero) {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            Document documento = null;

            try {
                DocumentBuilder builder = factory.newDocumentBuilder();
                DOMImplementation dom = builder.getDOMImplementation();
                documento = dom.createDocument(null,  "xml", null);

                Element raiz = documento.createElement(DATABASE_NAME);
                documento.getDocumentElement().appendChild(raiz);

                Element nodoObject1 = null, nodoObject = null, nodoDatos = null;
                Text texto = null;

                for (Object o : gestorObject) {
                    if(o.getClass().equals(Alumno.class)){
                        Alumno a = (Alumno) o;
                        nodoObject = documento.createElement(TAG_ALUMNO);
                        raiz.appendChild(nodoObject);

                        nodoDatos = documento.createElement(KEY_ID);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(String.valueOf(a.getId()));
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_NOMBRE);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(a.getNombre());
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_APELLIDOS);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(a.getApellido());
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_ALUMNO_EDAD);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(String.valueOf(a.getEdad()));
                        nodoDatos.appendChild(texto);

                    }
                    if(o.getClass().equals(Profesor.class)){
                        Profesor a = (Profesor) o;
                        nodoObject = documento.createElement(TAG_PROFESOR);
                        raiz.appendChild(nodoObject);

                        nodoDatos = documento.createElement(KEY_ID);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(String.valueOf(a.getId()));
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_NOMBRE);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(a.getNombre());
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_APELLIDOS);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(a.getApellido());
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_PROFESOR_EXPERIENCIA);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(String.valueOf(a.getExperiencia()));
                        nodoDatos.appendChild(texto);
                    }
                    if(o.getClass().equals(Asignatura.class)){
                        Asignatura a = (Asignatura) o;
                        nodoObject = documento.createElement(TAG_ASIGNATURA);
                        raiz.appendChild(nodoObject);

                        nodoDatos = documento.createElement(KEY_ID);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(String.valueOf(a.getId()));
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_NOMBRE);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(a.getNombre());
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_ASIG_DESCRIPCION);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(a.getDescripcion());
                        nodoDatos.appendChild(texto);

                        nodoDatos = documento.createElement(KEY_ASIGNATURA_HORAS);
                        nodoObject.appendChild(nodoDatos);
                        texto = documento.createTextNode(String.valueOf(a.getHoras()));
                        nodoDatos.appendChild(texto);
                    }

                }

                Source source = new DOMSource(documento);
                Result resultado = new StreamResult(fichero);

                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.transform(source, resultado);

            } catch (ParserConfigurationException pce) {
                pce.printStackTrace();
            } catch (TransformerConfigurationException tce) {
                tce.printStackTrace();
            } catch (TransformerException te) {
                te.printStackTrace();
            }   catch (NullPointerException npe){
                npe.printStackTrace();
            }
        }
        public static List<Object> importarXml(File fichero){
            ArrayList<Object> objects = new ArrayList<>();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            Document documento = null;


            try {
                DocumentBuilder builder = factory.newDocumentBuilder();
                documento = builder.parse(fichero);

                // Recorre cada uno de los nodos
                NodeList lista = documento.getElementsByTagName("*");
                for(int i = 0 ; i<lista.getLength(); i++){
                    Element nodoObjec = (Element)lista.item(i);
                    Element nodoObject1 = (Element)lista.item(i);
                    //tagname igual al que se usa para guardar los index sependen del orden de guardado
                    if(nodoObjec.getTagName().equals(TAG_ALUMNO)){
                        Alumno object = new Alumno();
                        //object.setId((ObjectId)Long.parseLong(nodoObjec.getChildNodes().item(0).getTextContent()));
                        object.setNombre(nodoObjec.getChildNodes().item(1).getTextContent());
                        object.setApellido(nodoObjec.getChildNodes().item(2).getTextContent());
                        object.setEdad(Integer.parseInt(nodoObjec.getChildNodes().item(3).getTextContent()));
                        objects.add(object);
                    }
                    if(nodoObjec.getTagName().equals(TAG_ASIGNATURA)){
                        Asignatura object = new Asignatura();
                        //object.setId(Integer.parseInt(nodoObjec.getChildNodes().item(0).getTextContent()));
                        object.setNombre(nodoObjec.getChildNodes().item(1).getTextContent());
                        object.setDescripcion(nodoObjec.getChildNodes().item(2).getTextContent());
                        object.setHoras(Integer.parseInt(nodoObjec.getChildNodes().item(3).getTextContent()));
                        objects.add(object);
                    }
                    if(nodoObjec.getTagName().equals(TAG_PROFESOR)){
                        Profesor object = new Profesor();
                        //object.setId(Integer.parseInt(nodoObjec.getChildNodes().item(0).getTextContent()));
                        object.setNombre(nodoObjec.getChildNodes().item(1).getTextContent());
                        object.setApellido(nodoObjec.getChildNodes().item(2).getTextContent());
                        object.setExperiencia(Integer.parseInt(nodoObjec.getChildNodes().item(3).getTextContent()));
                        objects.add(object);
                    }

                }

            } catch (ParserConfigurationException pce) {
                pce.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } catch (SAXException saxe) {
                saxe.printStackTrace();
            }

            return objects;
        }
}
